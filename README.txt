____________________

-- ESCAPE DUNGEON --
____________________

Pour lancer jeu, lancer l'excutable tp_unity3D.exe.
Voir https://gitlab.com/e-girls-who-code/escape-dungeon

_ _ _ _ _ _ _ _ _ _ _ _ _ _ 

[ Commandes ]

Utilisez ZQSD pour vous déplacer, et bouger la caméra à l'aide de votre souris.
Vous pouvez courir en utilisant SHIFT
Accroupissez vous en utlisant G, et relevez vous en appuyant sur H
Sautez en utlisant ESPACE
( assurez vous de bien utiliser ces commandes en vous déplaçant )
Intéragissez avec les clés en appuyant sur E.
Avec clic gauche lancez vos pièces.
_ _ _ _ _ _ _ _ _ _ _ _ _ _ 

[ Pitch du Jeu ]

Vous devez vous échappez de ce donjon. Pour cela, 3 clés sont cachées dans le niveau.
Trouver les 3 clés amène à la fin du jeu.
Se faire capturer par les gardes vous fait perdre.
Heureusement, les gardes sont avides d'argent, et seront si facilement distrait par les pièces
lancées, qu'ils préfèrent les ramasser avant de vous poursuivre ! 

